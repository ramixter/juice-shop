FROM node:18-slim as installer

COPY . /juice-shop
WORKDIR /juice-shop

# Consolidar comandos RUN para reducir capas
RUN npm i -g typescript ts-node && \
    npm install --omit=dev --unsafe-perm && \
    npm dedupe && \
    rm -rf frontend/node_modules frontend/.angular frontend/src/assets && \
    mkdir logs && \
    chown -R 65532 logs && \
    chgrp -R 0 ftp/ frontend/dist/ logs/ data/ i18n/ && \
    chmod -R g=u ftp/ frontend/dist/ logs/ data/ i18n/ && \
    rm -f data/chatbot/botDefaultTrainingData.json ftp/legal.md i18n/*.json

FROM node:18-slim

WORKDIR /juice-shop
COPY --from=installer --chown=65532:0 /juice-shop .

USER 65532
EXPOSE 3000
CMD ["/juice-shop/build/app.js"]

